bpd2
===

BPD2 is a [Beets](https://beets.io) plugin that implements enough of the MPD
protocol to work as a *[satellite](https://mpd.readthedocs.io/en/stable/user.html#satellite-setup)*
for another MPD instance.

The use case is wanting to use the Beets library remotely with a local MPD
instance (which has control over the local audio device), controlled locally
by any MPD client. Data is served over HTTP by an embedded server.

It is a minor modification of the [original bpd plugin](https://beets.readthedocs.io/en/stable/plugins/bpd.html)
meant to add support for protocol version 0.20, which is required by modern mpd
versions. It adds the following features:

* support for some additional v0.20 tagtypes in find/search queries
* an HTTP server that serves music files according to their vfs paths (so
  you don't have to necessarily move your files when importing to beets)

Note that the embedded HTTP server has no authentication and it is **not**
meant to be deployed publicly.

Support for protocol version 0.21 is in the works.

### Example setup

On the beets host:

```
beet bpd2
```

This will create, by default, an MPD server on port 6600 and an HTTP server on
port 6680.

On the other host, the mpd configuration will look something like

```
database {
  plugin "proxy"
  host "localhost"
  port "6601"
}
music_directory "http://localhost:6680/"
```

Port 6601 is being used to avoid conflict with the local *mpd* daemon. One should
then connect the local and remote ports, for instance with a SSH tunnel:

```
ssh -L 6601:localhost:6600 -L 6680:localhost:6680 otherhost
```

