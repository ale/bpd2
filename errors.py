from string import Template


RESP_ERR = 'ACK'

ERROR_NOT_LIST = 1
ERROR_ARG = 2
ERROR_PASSWORD = 3
ERROR_PERMISSION = 4
ERROR_UNKNOWN = 5
ERROR_NO_EXIST = 50
ERROR_PLAYLIST_MAX = 51
ERROR_SYSTEM = 52
ERROR_PLAYLIST_LOAD = 53
ERROR_UPDATE_ALREADY = 54
ERROR_PLAYER_SYNC = 55
ERROR_EXIST = 56


class BPDError(Exception):
    """An error that should be exposed to the client to the BPD
    server.
    """
    def __init__(self, code, message, cmd_name='', index=0):
        self.code = code
        self.message = message
        self.cmd_name = cmd_name
        self.index = index

    template = Template('$resp [$code@$index] {$cmd_name} $message')

    def response(self):
        """Returns a string to be used as the response code for the
        erring command.
        """
        return self.template.substitute({
            'resp':     RESP_ERR,
            'code':     self.code,
            'index':    self.index,
            'cmd_name': self.cmd_name,
            'message':  self.message,
        })


def make_bpd_error(s_code, s_message):
    """Create a BPDError subclass for a static code and message.
    """

    class NewBPDError(BPDError):
        code = s_code
        message = s_message
        cmd_name = ''
        index = 0

        def __init__(self):
            pass
    return NewBPDError

ArgumentTypeError = make_bpd_error(ERROR_ARG, 'invalid type for argument')
ArgumentIndexError = make_bpd_error(ERROR_ARG, 'argument out of range')
ArgumentNotFoundError = make_bpd_error(ERROR_NO_EXIST, 'argument not found')
