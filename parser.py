
TEST_QUERY = ' ( ( Author == "boia") AND(!(Title=~"de\\"")))'

STRING = 0
SYMBOL = 2

op_chars = '=!<>~'
quote_chars = "'\""
identifier_chars = \
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_'
valid_operators = ('==', '!=', '=~', '!~')
string_operators = ('contains', 'starts_with')


def lexer(s):
    state = None
    quotechar = None
    escaping = False
    accum = ''
    for c in s:
        if state == 'q':
            if not escaping and c == '\\':
                escaping = True
                continue
            if c != quotechar or escaping:
                accum += c
            else:
                yield (STRING, accum)
                accum = ''
                state = None
            escaping = False
            continue
        elif state == 'i':
            if c in identifier_chars:
                accum += c
                continue
            yield (STRING, accum)
            accum = ''
            state = None
        elif state == 'o':
            if c in op_chars:
                accum += c
                continue
            yield (SYMBOL, accum)
            accum = ''
            state = None

        if c in quote_chars:
            state = 'q'
            quotechar = c
        elif c in identifier_chars:
            state = 'i'
            accum += c
        elif c in op_chars:
            state = 'o'
            accum += c
        elif c != ' ':
            state = None
            yield (SYMBOL, c)

    yield (None, '')


class parser():
    """Parser for MPD 0.21 query strings.

    Associated grammar (right-recursive):

        Expr ::= ( STRING OP STRING ) | ( STRING STRING ) | ( Conj )
        BoolExpr ::= Neg
        Neg ::= Expr | ! Expr
        Conj ::= Neg | Neg AND Conj
        Query ::= Expr

    """

    def __init__(self, s):
        self.lex = lexer(s)
        self.current = next(self.lex)

    def accept_symbol(self, s):
        if self.current == (SYMBOL, s):
            self.current = next(self.lex)
            return True
        return False

    def accept_token(self, type, value):
        if self.current == (type, value):
            self.current = next(self.lex)
            return True
        return False

    def expect_symbol(self, s):
        if self.current == (SYMBOL, s):
            self.current = next(self.lex)
            return True
        raise Exception(
            f'Unexpected character {self.current[1]}, expected {s}')

    def identifier(self):
        if self.current[0] != STRING:
            raise Exception(f'Expected string, got "{self.current[1]}"')
        s = self.current[1]
        self.current = next(self.lex)
        return s

    def neg(self):
        if self.accept_symbol('!'):
            return 'NOT ' + self.expr()
        return self.expr()

    def conj(self):
        e = self.neg()
        if self.accept_token(STRING, 'AND'):
            e += ' AND '
            e += self.conj()
        return e

    def expr(self):
        self.expect_symbol('(')
        # Peek to determine which branch to take.
        if self.current[0] == STRING:
            result = self.inner_expr()
        else:
            result = self.conj()
        self.expect_symbol(')')
        return result

    def inner_expr(self):
        lhs = self.identifier()
        if lhs in ('base', 'modified-since'):
            rhs = self.identifier()
            return f'({lhs} {rhs})'
        typ, oper = self.current
        self.current = next(self.lex)
        if typ == SYMBOL:
            if oper not in valid_operators:
                raise Exception('Invalid operator {oper}')
        else:
            if oper not in string_operators:
                raise Exception('Invalid operator {oper}')
        rhs = self.identifier()
        return f'({lhs} {oper} {rhs})'

    def query(self):
        return self.expr()


if __name__ == '__main__':
    print(TEST_QUERY)
    #print(list(lexer(TEST_QUERY)))

    p = parser(TEST_QUERY)
    print(p.query())
