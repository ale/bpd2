import os
import threading

from beets import util
from beetsplug.bpd2.errors import ArgumentNotFoundError
from flask import Flask, abort, g, send_file


app = Flask(__name__)


@app.before_request
def before_request():
    g.lib = app.config['lib']
    g.resolve_path = app.config['resolve_path']


@app.route('/<path:fake_path>')
def serve(fake_path):
    try:
        item_id = g.resolve_path(fake_path)
    except ArgumentNotFoundError:
        abort(404)
    item = g.lib.get_item(item_id)
    item_path = util.py3_path(item.path)

    response = send_file(item_path)
    response.headers['Content-Length'] = os.path.getsize(item_path)
    return response


def start_app(resolve_path_fn, lib, host, port):
    app.config['resolve_path'] = resolve_path_fn
    app.config['lib'] = lib
    thread = threading.Thread(
        target=app.run,
        kwargs=dict(host=host, port=port, threaded=True),
        daemon=True)
    thread.start()
